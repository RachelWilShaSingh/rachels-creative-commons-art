# Playdate

https://play.date/

Resolution: 400x240

* 40x40 pixels: 10x6
* 20x20 pixels: 20x12
* 10x10 pixels: 40x24
* 5x5 pixels: 80x48

# License

These tiles are all **Public Domain**. If you want to credit me,
please credit "Rachel Singh". Otherwise, you don't have to worry about it!

# Images

**Basic UI:**

![basic_ui.png](basic_ui.png)

**Puzzle pieces:**

![20x20_puzzle_pieces.png](20x20_puzzle_pieces.png)

**20x20 tileset:**

![20x20_tileset.png](20x20_tileset.png)

# Examples

![examplemaps/map1.png](examplemaps/map1.png)

![examplemaps/map2.png](examplemaps/map2.png)
